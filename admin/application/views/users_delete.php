          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">{user_realname}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <a href="<?php echo base_url("user/create"); ?>" class="btn btn-sm btn-outline-secondary">Create User</a>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
              </button>
            </div>
          </div>
User ID: {user_id}<br />
E-Mail: {user_email}<br />

<h3>ARE YOU SURE YOU WANT TO DELETE THIS USER?</h3>

<form method="post">
  <button type="submit" class="btn btn-danger" name="delete">Delete</button>
  <a href="{cancel_link}" class="btn btn-info">Cancel</a>
</form>
