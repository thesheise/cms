<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">City Index</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group mr-2">
      <a href="{create_link}" class="btn btn-sm btn-outline-secondary">Create City</a>
      <button class="btn btn-sm btn-outline-secondary">Export</button>
    </div>
    <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
      <span data-feather="calendar"></span>
      This week
    </button>
  </div>
</div>

<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">City name</th>
      <th scope="col">Description</th>
      <th scope="col">City population</th>
      <th scope="col">Slug</th>
    </tr>
  </thead>
    <tbody>
      {cities}
      <tr>
        <td>{city_id}</td>
        <td>{city_name}</td>
        <td>{city_desc}</td>
        <td>{city_pop}</td>
        <td>{city_slug}</td>
      <td>

              <div class="btn-group mr-2">
                <a href="{readlink}" class="btn btn-sm btn-outline-secondary">Read</a>
                <a href="{updatelink}" class="btn btn-sm btn-outline-secondary">Edit</a>
                <a href="{deletelink}" class="btn btn-sm btn-outline-secondary">Delete</a>
              </div>


        </td>
      </tr>
      {/cities}
    </tbody>
</table>
