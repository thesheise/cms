<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Images</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group mr-2">
      <a href="<?php echo base_url("images/create"); ?>" class="btn btn-sm btn-outline-secondary">Upload Image</a>
      <button class="btn btn-sm btn-outline-secondary">Export</button>
    </div>
    <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
      <span data-feather="calendar"></span>
      This week
    </button>
  </div>
</div>

{images}
<div class="col-4" style="text-align:center;">
  <img src="{image_file}" class="img-fluid" alt="Responsive image"><br />
</div>
  <div style="margin-top:5px; margin-bottom:5px;" class="btn-group mr-2">
    <a href="{readlink}" class="btn btn-sm btn-outline-secondary">View</a>
    <a href="{updatelink}" class="btn btn-sm btn-outline-secondary">Update</a>
    <a href="{deletelink}" class="btn btn-sm btn-outline-secondary">Delete</a>
</div>
{/images}
