<form class="form-horizontal" method="post">
<fieldset>

<!-- Form Name -->
<legend>Page Create</legend>

{validation_errors}

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="page_headline">Headline</label>
  <div class="col-md-4">
  <input id="page_headline" name="page_headline" type="text" placeholder="Headline" class="form-control input-md">

  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="page_content">Content</label>
  <div class="col-md-4">
    <textarea class="form-control" id="page_content" name="page_content"></textarea>
  </div>
</div>


<!-- Multiple Checkboxes -->
<div class="form-group">
  <label class="col-md-4 control-label" for="page_frontpage">Options</label>
  <div class="col-md-4">
  <div class="checkbox">
    <label for="page_frontpage-0">
      <input type="checkbox" name="page_frontpage" id="page_frontpage-0" value="1">
      Frontpage
    </label>
	</div>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="page_submit"></label>
  <div class="col-md-4">
    <button type="submit" id="page_submit" name="page_submit" class="btn btn-success">Create Page</button>
  </div>
</div>

</fieldset>
</form>
