          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Update User</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <a href="<?php echo base_url("user/create"); ?>" class="btn btn-sm btn-outline-secondary">Create User</a>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
              </button>
            </div>
          </div>



<form class="form-horizontal" method="post">
<fieldset>

{validation_errors}

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="user_email">E-Mail</label>
  <div class="col-md-4">
  <input id="user_email" name="user_email" type="text" placeholder="E-Mail" class="form-control input-md" value="{user_email}">

  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="user_password">Password</label>
  <div class="col-md-4">
    <input id="user_password" name="user_password" type="password" placeholder="Password" class="form-control input-md">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="user_realname">Real Name</label>
  <div class="col-md-4">
  <input id="user_realname" name="user_realname" type="text" placeholder="Real Name" class="form-control input-md" value="{user_realname}">

  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="user_level">Level</label>
  <div class="col-md-4">
    <select id="user_level" name="user_level" class="form-control">
      <option value="0">Regular User</option>
      <option value="1">Admin</option>
    </select>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="formSubmit"></label>
  <div class="col-md-4">
    <button type="submit" id="formSubmit" name="formSubmit" class="btn btn-info">Update User</button>
  </div>
</div>

</fieldset>
</form>
