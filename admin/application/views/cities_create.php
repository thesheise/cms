
<form class="form-horizontal" method="post" enctype="multipart/form-data">
<fieldset>

<!-- Form Name -->
<legend>Create City</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="city_name">City Name</label>
  <div class="col-md-4">
  <input id="city_name" name="city_name" type="text" placeholder="" class="form-control input-md">
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="city_desc">Description</label>
  <div class="col-md-4">
    <textarea class="form-control" id="city_desc" name="city_desc"></textarea>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="city_name">City Population</label>
  <div class="col-md-4">
  <input id="city_pop" name="city_pop" type="text" placeholder="" class="form-control input-md">
  </div>
</div>

<!-- City Image -->
<div class="form-group">
  <label class="col-md-4 control-label" for="formFile">City Image</label>
  <div class="col-md-4">
    <input id="formFile" name="formFile" class="input-file" type="file">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="page_submit"></label>
  <div class="col-md-4">
    <button type="submit" id="page_submit" name="page_submit" class="btn btn-success">Create City</button>
  </div>
</div>

</fieldset>
</form>
