<form class="form-horizontal" method="post" enctype="multipart/form-data">
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- File Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="formFile"></label>
  <div class="col-md-4">
    <input id="formFile" name="formFile" class="input-file" type="file">
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="formSubmit"></label>
  <div class="col-md-4">
    <button type="submit" id="formSubmit" name="formSubmit" class="btn btn-default">Upload</button>
  </div>
</div>

</fieldset>
</form>
