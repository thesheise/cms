<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">{city_name}</h1>
</div>

<p>{city_desc}</p>


<div class="col-4" style="text-align:center;">
  <img src="{city_img}" class="img-fluid" alt="Responsive image"><br />
</div>
