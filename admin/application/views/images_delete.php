<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Delete Image</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group mr-2">
      <a href="<?php echo base_url("images/create"); ?>" class="btn btn-sm btn-outline-secondary">Upload Image</a>
      <button class="btn btn-sm btn-outline-secondary">Export</button>
    </div>
    <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
      <span data-feather="calendar"></span>
      This week
    </button>
  </div>
</div>

<img src="{image_file}" class="img-fluid" alt="Responsive image"><br />
<p>{image_description}</p>
<h4>ARE YOU SURE YOU WANT TO DELETE THIS IMAGE?</h4>

<form method="post">
<button type="submit" class="btn btn-danger" name="delete">Delete</button>
<a href="{cancel_link}" class="btn btn-info">Cancel</a>
</form>
