          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Page List</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <a href="{create_link}" class="btn btn-sm btn-outline-secondary">Create Page</a>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
              </button>
            </div>
          </div>


<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Headline</th>
      <th scope="col">Slug</th>
      <th scope="col">Author</th>
      <th scope="col">Published</th>
      <th scope="col">Frontpage</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
    <tbody>
      {pages}
      <tr>
        <td>{page_id}</td>
        <td>{page_headline}</td>
        <td>{page_slug}</td>
        <td>{page_author}</td>
        <td>{page_published}</td>
        <td>{page_frontpage}</td>
        <td>

              <div class="btn-group mr-2">
                <a href="{readlink}" class="btn btn-sm btn-outline-secondary">Read</a>
                <a href="{updatelink}" class="btn btn-sm btn-outline-secondary">Edit</a>
                <a href="{deletelink}" class="btn btn-sm btn-outline-secondary">Delete</a>
              </div>
          

        </td>
      </tr>
      {/pages}
    </tbody>
</table>