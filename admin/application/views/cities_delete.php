<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
	<h1 class="h2">{city_name}</h1>
</div>

<p>{city_desc}</p>

<h3>ARE YOU SURE YOU WANT TO DELETE THIS CITY?</h3>

<form method="post">
	<button type="submit" class="btn btn-danger" name="delete">Delete</button>
	<a href="{cancel_link}" class="btn btn-info">Cancel</a>
</form>
