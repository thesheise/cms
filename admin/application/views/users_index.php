          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">User List</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <a href="{create_link}" class="btn btn-sm btn-outline-secondary">Create User</a>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
              </div>
              <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
              </button>
            </div>
          </div>


<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">E-Mail</th>
      <th scope="col">Real Name</th>
      <th scope="col">Level</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
    <tbody>
      {users}
      <tr>
        <td>{user_id}</td>
        <td>{user_email}</td>
        <td>{user_realname}</td>
        <td>{user_level}</td>
        <td>

              <div class="btn-group mr-2">
                <a href="{readlink}" class="btn btn-sm btn-outline-secondary">Read</a>
                <a href="{updatelink}" class="btn btn-sm btn-outline-secondary">Edit</a>
                <a href="{deletelink}" class="btn btn-sm btn-outline-secondary">Delete</a>
              </div>


        </td>
      </tr>
      {/users}
    </tbody>
</table>
