<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Upload Image</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <div class="btn-group mr-2">
      <button class="btn btn-sm btn-outline-secondary">Export</button>
    </div>
    <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
      <span data-feather="calendar"></span>
      This week
    </button>
  </div>
</div>


<form class="form-horizontal" method="post" enctype="multipart/form-data">
<fieldset>


<!-- File Button -->
<div class="form-group">
  <div class="col-md-4">
    <input id="formFile" name="formFile" class="input-file" type="file">
  </div>
</div>

<!--Textarea-->
<div class="form-group">
  <label class="col-md-4 control-label" for="image_description">Description</label>
  <div class="col-md-4">
    <textarea class="form-control" id="image_description" name="image_description"></textarea>
  </div>
</div>



<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="formSubmit"></label>
  <div class="col-md-4">
    <button type="submit" id="formSubmit" name="formSubmit" class="btn btn-success">Upload Image</button>
  </div>
</div>

</fieldset>
</form>
