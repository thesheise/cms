<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function index(){
		if(!$this->session->user){
			redirect('users/login');
		}
		$data = array(
						'create_link'		=>	base_url('users/create')
		);

		$data['users'] = $this->db->get('users')->result_array();
		foreach($data['users'] as $key => $value){
			$data['users'][$key]['readlink'] = base_url('users/read/'.$value['user_id']);
			$data['users'][$key]['updatelink'] = base_url('users/update/'.$value['user_id']);
			$data['users'][$key]['deletelink'] = base_url('users/delete/'.$value['user_id']);
			if($data['users'][$key]['user_level'] == 1){
				$data['users'][$key]['user_level'] = "<img src='".base_url('assets/img/admin.png')."' />";
			}else{
				$data['users'][$key]['user_level'] = "";
			}
		}

		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('users_index',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}






// USER CREATE FUCTION
	public function create(){
		if(!$this->session->user){
			redirect('users/login');
		}

		if($this->input->post()){
			$this->form_validation->set_rules('user_email','E-Mail','required|valid_email');
			$this->form_validation->set_rules('user_password','Password','required|min_length[6]');
			if($this->form_validation->run()){

				$user_data 	= 	array(
									'user_email'		=>	$this->input->post('user_email'),
									'user_password'		=>	password_hash($this->input->post('user_password'), PASSWORD_DEFAULT),
									'user_level'		=>	$this->input->post('user_level')
								);

				if(!empty($this->input->post('user_realname'))){
					$user_data['user_realname']	= $this->input->post('user_realname');
				}

				$this->db->insert('users',$user_data);


				$config['upload_path'] = FCPATH."assets/img/users";
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['encrypt_name'] = TRUE;

				$this->load->library('upload',$config);

				if($this->upload->do_upload('formFile')){
					$profilepicture = array(
										'pp_uid'	=>	$this->db->insert_id('users'),
										'pp_image'	=>	$this->upload->data('file_name')
										);
					$this->db->insert('profilepictures',$profilepicture);
				}

				redirect('users');

			}
		}

		$data = array(
					'validation_errors'		=>	validation_errors()
		);
		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('users_create',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}





// READ USER FUNCTION
	public function read($user_id){
		if(!$this->session->user){
			redirect('users/login');
		}
		$data = $this->db->where('user_id',$user_id)->get('users')->row_array();
		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('users_read',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}






// UPDATE USER FUNCTION
	public function update($user_id){
		if(!$this->session->user){
			redirect('users/login');
		}

		if($this->input->post()){
			$this->form_validation->set_rules('user_email','E-Mail','required|valid_email');
			if(!empty($this->input->post('user_password'))){
				$this->form_validation->set_rules('user_password','Password','required|min_length[6]');
			}
			if($this->form_validation->run()){
				$user_data 	= 	array(
									'user_email'		=>	$this->input->post('user_email'),
									'user_level'		=>	$this->input->post('user_level')
								);
				if(!empty($this->input->post('user_password'))){
					$user_data['user_password']		=	password_hash($this->input->post('user_password'), PASSWORD_DEFAULT);
				}
				if(!empty($this->input->post('user_realname'))){
					$user_data['user_realname']	= $this->input->post('user_realname');
				}
				$this->db->where('user_id',$user_id)->update('users',$user_data);
				redirect('users');
			}
		}


		$data = $this->db->where('user_id',$user_id)->get('users')->row_array();
		$data['validation_errors'] = validation_errors();


		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('users_update',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}





//USER DELETE FUNCTION
	public function delete($user_id){
		if(!$this->session->user){
			redirect('users/login');
		}

		if($this->input->post()){
			$this->db->where('user_id',$user_id)->delete('users');
			redirect('users');
		}


		$data = $this->db->where('user_id',$user_id)->get('users')->row_array();

		$data['cancel_link'] = base_url('users');
		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('users_delete',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}





//USER UPLOAD FUCTION
	public function upload(){

		if($this->input->post()){
			$config['upload_path'] = FCPATH."assets/img/users";
			$config['allowed_types'] = 'gif|jpg|png';
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload',$config);

			if($this->upload->do_upload('formFile')){
				echo "<pre>";
					print_r($this->upload->data('file_name'));
				echo "</pre>";
			}else{
				echo $this->upload->display_errors();
			}
		}


		$this->load->view('upload');
	}






//USER LOGIN FUNCTION - SESSION ENABLE
	public function login(){

		if($this->input->post()){
			$this->form_validation->set_rules('user_email','E-Mail','required|valid_email');
			$this->form_validation->set_rules('user_password','Password','required');
			if($this->form_validation->run()){
				$user = $this->db->where('user_email',$this->input->post('user_email'))->get('users')->row_array();

				if(password_verify($this->input->post('user_password'),$user['user_password'])){
					unset($user['user_password']);
					$this->session->set_userdata('user',$user);
					redirect();
				}
			}
		}

		$data = array(
					'validation_errors'		=>	validation_errors()
				);
		$this->parser->parse('users_login',$data);
	}





//USER LOGOUT FUNCTION - SESSION DESTROY
	public function logout(){
		$this->session->sess_destroy();
		redirect('users/login');
	}

}
