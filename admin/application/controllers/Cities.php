<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cities extends CI_Controller {


//Hvis du ikke er logget ind -> redirect til denne side
	public function __construct(){
		parent::__construct();
		if(!$this->session->user){
			redirect('users/login');
		}
	}


	public function index(){

		$data = array(
						'create_link'		=>	base_url('cities/create')
		);

		$data['cities'] = $this->db->get('cities')->result_array();

		foreach($data['cities'] as $key => $value){
			$data['cities'][$key]['readlink'] = base_url('cities/read/'.$value['city_id']);
			$data['cities'][$key]['updatelink'] = base_url('cities/update/'.$value['city_id']);
			$data['cities'][$key]['deletelink'] = base_url('cities/delete/'.$value['city_id']);
		}



		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('cities_list',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}

	public function create(){

		if($this->input->post()){
			$this->form_validation->set_rules('city_name','City Name','required|min_length[2]|max_length[100]');
			if($this->form_validation->run()){

				$city_data = array(
									'city_name'		=>	$this->input->post('city_name'),
									'city_desc'		=>	$this->input->post('city_desc'),
									'city_pop'		=>	$this->input->post('city_pop'),
									'city_slug'		=>	url_title($this->input->post('city_name'),'dash',TRUE),

									);


			$config['upload_path'] = FCPATH."admin/assets/img/";
			$config['allowed_types'] = "png|jpg|jpeg|gif";
			$config['encrypt_name'] = true;


// Function error - doesn't upload
			$this->load->library('upload', $config);


		if($this->upload->do_upload('formFile')){
				$image = array(
					'city_id'  	=> $this->input->post('city_id'),
					'city_img'  => $this->upload->data('file_name')
											);

				$this->db->insert('images', $image);
			}

				$this->db->insert('cities',$city_data);
			}
		}

		$data = array(
						'validation_errors'	=>	validation_errors()
		);

		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('cities_create',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}

	public function read($city_id){

		$this->db->where('city_id',$city_id);
		$dataCity = $this->db->get('cities')->row();
		$data["city_img"] = base_url("/assets/img/".$dataCity->city_img); // DOESNT WORK //

		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('cities_read',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}

	public function update($city_id){

		if($this->input->post()){
			$this->form_validation->set_rules('city_name','city name','required|min_length[2]|max_length[150]');
			if($this->form_validation->run()){

				$city_data = 		array(
										'city_name'		=>	$this->input->post('city_name'),
										'city_desc'		=>	$this->input->post('city_desc'),
										'city_pop'		=>	$this->input->post('city_pop'),
										'city_slug'		=>	url_title($this->input->post('city_name'),'dash',TRUE)
										);

				$this->db->where('city_id',$city_id);
				$this->db->update('cities',$city_data);
				redirect('cities');

				}
			}


		$data = $this->db->where('city_id',$city_id)->get('cities')->row_array();


		$data['validation_errors'] = validation_errors();

		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('cities_update',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}



	public function delete($city_id){

		if($this->input->post()){
			$this->db->where('city_id',$city_id);
			$this->db->delete('cities');
			redirect('cities');
		}

		$this->db->where('city_id',$city_id);
		$data = $this->db->get('cities')->row_array();

		$data['cancel_link'] = base_url('cities');

		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('cities_delete',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}

}
