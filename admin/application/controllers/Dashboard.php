<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


//Hvis du ikke er logget ind -> redirect til denne side
	public function __construct(){
		parent::__construct();
		if(!$this->session->user){
			redirect('users/login');
		}
	}


	public function index(){
		$data = array();
		$this->parser->parse('template/header',$data);
		$this->parser->parse('template/topbar',$data);
		$this->parser->parse('template/nav',$data);
		$this->parser->parse('template/mainstart',$data);
		$this->parser->parse('dashboard',$data);
		$this->parser->parse('template/mainend',$data);
		$this->parser->parse('template/footer',$data);
	}
}
