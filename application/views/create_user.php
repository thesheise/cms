        <div class="col-md-8 blog-main">


<form class="form-horizontal" method="post">
<fieldset>

<!-- Form Name -->
<legend>Create User</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="user_email">User E-Mail</label>  
  <div class="col-md-4">
  <input id="user_email" name="user_email" type="text" placeholder="User E-Mail" class="form-control input-md" value="{value_email}"><br />
  <span style="color: red;">{error_email}</span>
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="user_password">User Password</label>
  <div class="col-md-4">
    <input id="user_password" name="user_password" type="password" placeholder="User Password" class="form-control input-md"><br />
    <span style="color: red;">{error_password}</span>
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="user_password_repeat">Repeat User Password</label>
  <div class="col-md-4">
    <input id="user_password_repeat" name="user_password_repeat" type="password" placeholder="Repeat User Password" class="form-control input-md"><br />
    <span style="color: red;">{error_password_repeat}</span>
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="user_submit"></label>
  <div class="col-md-4">
    <button type="submit" id="user_submit" name="user_submit" class="btn btn-success">Create User</button>
  </div>
</div>

</fieldset>
</form>




        </div><!-- /.blog-main -->
