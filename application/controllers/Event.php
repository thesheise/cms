<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

/* (SHOW INFO)

	public function __construct(){
		parent::__construct();
		echo "<pre>";
			print_r($this->session->all_userdata());
		echo "</pre>";
		echo "<pre>";
			print_r($this->input->post());
		echo "</pre>";
	}
	*/

	public function index($page_slug = null){

		if($page_slug){
			$this->db->where('page_slug',$page_slug);
		}

		$sqlQuery = $this->db->get('pages');

		$data = $sqlQuery->row_array();

		$data['nav'] = $this->db->get('pages')->result_array();

		#echo "<pre>";
		#	print_r($data);
		#echo "</pre>";

		$this->parser->parse('template/events',$data);
	}
