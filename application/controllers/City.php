<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {



public function index($city_slug = null){

	if($city_slug){
		$this->db->where('city_slug',$city_slug);
	}else{
		$this->db->where('page_frontpage',1);
	}

	$sqlQuery = $this->db->get('cities');

	$data = $sqlQuery->row_array();

	$data['nav'] = $this->db->get('pages')->result_array();

	#echo "<pre>";
	#	print_r($data);
	#echo "</pre>";

	$this->parser->parse('template/citiesheader',$data);
	$this->parser->parse('template/citiesmainstart',$data);
	$this->parser->parse('template/citiesmainend',$data);
	$this->parser->parse('template/citiesfooter',$data);
	}

}
